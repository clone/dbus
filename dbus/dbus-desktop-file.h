/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*- */
/* desktop-file.h  .desktop file parser
 *
 * Copyright (C) 2003  CodeFactory AB
 *
 * Licensed under the Academic Free License version 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
#ifndef BUS_DESKTOP_FILE_H
#define BUS_DESKTOP_FILE_H

#include <dbus/dbus.h>
#include <dbus/dbus-string.h>

#define DBUS_DESKTOP_PARSE_ERROR_INVALID_SYNTAX  "org.freedesktop.DBus.DesktopParseError.InvalidSyntax"
#define DBUS_DESKTOP_PARSE_ERROR_INVALID_ESCAPES "org.freedesktop.DBus.DesktopParseError.InvalidEscapes"
#define DBUS_DESKTOP_PARSE_ERROR_INVALID_CHARS   "org.freedesktop.DBus.DesktopParseError.InvalidChars"

typedef struct DBusDesktopFile DBusDesktopFile;

DBusDesktopFile *_dbus_desktop_file_load (DBusString      *filename,
				          DBusError       *error);
void            _dbus_desktop_file_free  (DBusDesktopFile *file);

dbus_bool_t _dbus_desktop_file_get_raw    (DBusDesktopFile *desktop_file,
					   const char      *section_name,
					   const char      *keyname,
					   const char     **val);
dbus_bool_t _dbus_desktop_file_get_string (DBusDesktopFile *desktop_file,
					   const char      *section,
					   const char      *keyname,
					   char           **val,
					   DBusError       *error);

const char *_dbus_desktop_file_get_section (DBusDesktopFile *desktop_file,
                                            unsigned         i);

dbus_bool_t _dbus_desktop_file_changed (DBusDesktopFile *desktop_file,
                                        DBusString      *filename);

#endif /* BUS_DESKTOP_FILE_H */
